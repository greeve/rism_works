import argparse
import csv
import requests


URL = 'https://viaf.org/viaf/sourceID/DE663|pe{}/viaf.xml'


def get_data(filename):
    author_ids = set()
    with open(filename) as fin:
        reader = csv.DictReader(fin, dialect='excel-tab')
        for row in reader:
            author_id = row['100$0']
            author_ids.add(author_id)
    return author_ids


def get_argument_parser():
    parser = argparse.ArgumentParser(
        description='Download VIAF records for people from RISM',
    )
    parser.add_argument('filename', help='A csv file')

    return parser


def main():
    parser = get_argument_parser()
    args = parser.parse_args()
    author_ids = get_data(args.filename)
    for author_id in author_ids:
        r = requests.get(URL.format(author_id))
        print(author_id, len(r.content))
        with open('viaf_output/' + author_id + '.xml', 'wb') as fout:
            fout.write(r.content)


if __name__ == '__main__':
    main()
