import argparse
import csv
import datetime
import pathlib

from collections import defaultdict
from xml.etree import ElementTree as ET

from fuzzywuzzy import fuzz

RISM_FIELDS = [
    '240$a',
    '240$n',
    '245$a',
    '100$a',
    '100$d',
    '100$0',
    '383$b',
    '593$a',
    '593$8',
    '035$a',
]


def rism_to_persons(rism):
    """
    Restructure RISM csv data to dictionary collating by persons
    """
    persons = defaultdict(list)
    with open(rism) as fin:
        reader = csv.DictReader(fin, dialect='excel-tab')
        for row in reader:
            rism_id = row['100$0']
            if rism_id:
                persons[rism_id].append(list(row.items()))
    return persons


def match_works_with_titles(filepath, persons):
    ns = {'ns1': 'http://viaf.org/viaf/terms#'}
    matches = []

    for rism_id, data in persons.items():
        tree = ET.parse(filepath / (rism_id + '.xml'))

        try:
            viaf_id = tree.find('./ns1:viafID', ns).text
            main_heading = tree.find(
                './ns1:mainHeadings/ns1:data/ns1:text', ns
            ).text
        except Exception:
            viaf_id = None
            main_heading = None
            print(rism_id)

        works = tree.findall('./ns1:titles/ns1:work', ns)

        for row in data:
            rism_title = row[2][1]
            rism_uniform_title = row[0][1]

            for work in works:
                title = work.find('ns1:title', ns).text
                sources = [s.text for s in work.findall('ns1:sources/ns1:sid', ns)]
                viaf_work_id = work.attrib.get('id')

                if viaf_work_id:
                    viaf_work_id = viaf_work_id.lstrip('VIAF|')
                    match = False

                    if title in rism_title:
                        match = True

                    if title in rism_uniform_title:
                        match = True

                    if rism_uniform_title in title:
                        match = True

                    if match:
                        row.append(('viaf_work_id', viaf_work_id))
                        row.append(('viaf_title', title))
                        row.append(('viaf_main_heading', main_heading))

                    # TODO Add fuzzy matching
                    # ratio = fuzz.ratio(title, rism_title)
                    # print(ratio)
                    # if ratio > 10:
                    #     print("Ratio match! ||| ", title + ' ||| ' + rism_title)
                    #     match = True

            matches.append(dict(row))

    return matches


def get_argument_parser():
    parser = argparse.ArgumentParser(
        description='Match VIAF works with RISM titles',
    )
    parser.add_argument('filepath', help='A folder with viaf xml files')
    parser.add_argument('rism', help='A file with RISM titles')
    return parser


def main():
    parser = get_argument_parser()
    args = parser.parse_args()

    filepath = pathlib.Path(args.filepath)
    persons = rism_to_persons(args.rism)
    data = match_works_with_titles(filepath, persons)
    filename = 'rism_titles_with_viaf_works_{}.csv'.format(
        datetime.date.today()
    )
    fields = list(data[0].keys())

    fields.append('viaf_work_id')
    fields.append('viaf_title')
    fields.append('viaf_main_heading')

    with open(filename, 'w') as fout:
        writer = csv.DictWriter(fout, fields, dialect='excel-tab')
        writer.writeheader()
        writer.writerows(data)


if __name__ == '__main__':
    main()
