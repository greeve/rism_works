# RISM

A repository of data and scripts to research extracting and reconciling work/title information from [RISM music catalog](http://rism.info) records.

# Stand-alone Procedure

1. Download the [RISM catalog data](https://opac.rism.info/index.php?id=10) in MARCXML format.
2. Convert MARCXML to MARC format using [MarcEdit](https://marcedit.reeset.net).
3. Use [MarcEdit](https://marcedit.reeset.net) to find all harp-related catalog records (e.g. search for "arp" in the 594 field) and create a MARC file with the results. 
4. Run `output_titles.py` using the output from step 3 as input to get a list of RISM titles with associated metadata.
5. Run `download_viaf_persons.py` using the output from step 4 as input to download VIAF person cluster XML files based on the RISM person ID that is connected to the VIAF person cluster. 
6. Run `match_viaf_works.py` using the output files from step 5 and the output RISM data from step 4 to match VIAF works to RISM titles. 
7. Analyze the output in tools such as Excel or OpenRefine. The output from step 6 can be used to identify RISM catalog records that have a matching VIAF work ID versus those records that don't and may be candidates as new works. 

# Proposed OpenRefine Procedure

1. Download the [RISM catalog data](https://opac.rism.info/index.php?id=10) in MARCXML format.
2. Convert MARCXML to MARC format using [MarcEdit](https://marcedit.reeset.net).
3. Use [MarcEdit](https://marcedit.reeset.net) to find all harp-related catalog records (e.g. search for "arp" in the 594 field) and create a MARC file with the results. 
4. Run `output_titles.py` using the output from step 3 as input to get a list of RISM titles with associated metadata.
5. Create a new project in OpenRefine using the csv output in step 4.
6. Add the RISM-VIAF reconciliation service to OpenRefine.
7. Reconcile title information in the RISM 240 or 245 fields using the RISM-VIAF reconciliation service to find matching works, review potential matches, and identify new works.

# Details about getting VIAF work data using the RISM person ID

Each 100 field in the RISM catalog records contains a 0 subfield with the RISM ID for that person in the RISM authority file. This RISM person ID can be used to find the associated VIAF person cluster by using the following URL template: 

`http://viaf.org/viaf/sourceID/DE663|pe<rism_person_id>`

For example, the RISM ID 30002218 is an identifier for Millico, Giuseppe, 1737-1802. Using this identifier the following URL will show the corresponding VIAF person cluster for that person.

<http://viaf.org/viaf/sourceID/DE663|pe30002218>

Each VIAF person cluster contains a list of works. Works with a VIAF ID are works that have a VIAF work cluster. The title of these VIAF works can be compared to the title found in the original RISM record. The VIAF work cluster ID is added to the RISM record with a matching title. These matches can be used as a starting point for disambiguating works within RISM catalog records. 

# Manifest

## Folders 
- `bin/`: Contains output genereated from the scripts
- `examples/`: Contains sample files used to plan these set of scripts

## Files

- `marcxml_to_mrc.py`: A Python script for converting MARCXML into MARC
- `output_titles.py`: A Python script for extracting title information from MARC OPAC catalog data
- `download_viaf_persons.py`: A Python script for downloading VIAF person cluster data based on the RISM ID for the person
- `get_viaf_works.py`: A Python script for extracting out the VIAF works for each person cluster source file downloaded using `download_viaf_person.py`.
- `match_viaf_works.py`: A Python script for matching VIAF works to RISM titles.
- `combine_rism_viaf.py`: A Python script for combining RISM title information with VIAF work information (i.e. combine the output from `output_titles.py` and `get_viaf_works.py`)
- `requirements.txt``: A text file containing the required packages for running these Python scripts

# Notes

- how the RISM person id is being used to get the viaf work data for a given person cluster
- give the csv file for the openrefine project
- how we are using the openrefine project
- openrefine screenshot
- viaf screenshots of examples
