import argparse
import csv
import datetime


FIELDS = [
    '100$a',
    '100$d',
    '100$0',
    '240$a',
    '240$n',
    '245$a',
    '383$b',
    'viaf_person_id',
    'viaf_work_id',
    'rism_source',
    'lc_source',
]


def get_argument_parser():
    parser = argparse.ArgumentParser(
        description='Combine the output of RISM titles with VIAF titles',
    )
    parser.add_argument('rism', help='RISM title output')
    parser.add_argument('viaf', help='VIAF title output')

    return parser


def main():
    parser = get_argument_parser()
    args = parser.parse_args()
    rism_data = None
    with open(args.rism) as fin:
        reader = csv.DictReader(fin, dialect='excel-tab')
        rism_data = list(reader)

    viaf_data = None
    with open(args.viaf) as fin:
        reader = csv.DictReader(fin, dialect='excel-tab')
        viaf_data = list(reader)

    for row in viaf_data:
        row['100$0'] = row['rism_person_id']
        row.pop('rism_person_id')
        row['245$a'] = row['title']
        row.pop('title')

    filename = 'combined_titles_{}.csv'.format(datetime.date.today())
    with open(filename, 'w') as fout:
        writer = csv.DictWriter(fout, FIELDS, dialect='excel-tab')
        writer.writeheader()
        writer.writerows(rism_data)
        writer.writerows(viaf_data)


if __name__ == '__main__':
    main()
