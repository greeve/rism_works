import argparse
import csv
import datetime

import pymarc


def get_data(filename):
    data = []
    with open(filename, 'rb') as fin:
        reader = pymarc.MARCReader(fin, utf8_handling='replace')
        for record in reader:
            print(record)
            author = record['100']['a']
            author_date = record['100']['d']
            author_rism_id = record['100']['0']
            if record['383']:
                num_designation = record['383']['b']
            else:
                num_designation = None

            print_source = record['035']
            if print_source:
                print_source = print_source['a']

            row = {
                '240$a': record['240']['a'],
                '240$n': record['240']['n'],
                '245$a': record['245']['a'],
                '100$a': author,
                '100$d': author_date,
                '100$0': author_rism_id,
                '383$b': num_designation,
                '593$a': record['593']['a'],
                '593$8': record['593']['8'],
                '035$a': print_source,
            }

            data.append(row)
    return data


def get_argument_parser():
    parser = argparse.ArgumentParser(
        description='Print the titles of MARC records',
    )
    parser.add_argument('filename', help='A MARC file')

    return parser


def main():
    parser = get_argument_parser()
    args = parser.parse_args()
    data = get_data(args.filename)
    filename = 'rism_titles_{}.csv'.format(datetime.date.today())
    with open(filename, 'w') as fout:
        writer = csv.DictWriter(fout, data[0].keys(), dialect='excel-tab')
        writer.writeheader()
        writer.writerows(data)


if __name__ == '__main__':
    main()
