import argparse

import pymarc


def marcxml_to_marc(filename):
    output = filename.replace('.xml', '.mrc')
    writer = pymarc.MARCWriter(open(output, 'wb'))
    pymarc.map_xml(writer.write, filename)
    writer.close()


def get_argument_parser():
    parser = argparse.ArgumentParser(
        description='A script to convert a MARCXML file to MARC',
    )
    parser.add_argument('filename', help='A MARCXML file')

    return parser


def main():
    parser = get_argument_parser()
    args = parser.parse_args()
    marcxml_to_marc(args.filename)


if __name__ == '__main__':
    main()
