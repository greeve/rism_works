import argparse
import csv
import datetime
import pathlib

from xml.etree import ElementTree as ET


def get_data(filepath):
    ns = {'ns1': 'http://viaf.org/viaf/terms#'}
    data = []
    for f in filepath.iterdir():
        tree = ET.parse(f)
        viaf_person_id = tree.find('.//ns1:viafID', ns)
        result = tree.findall('.//ns1:work', ns)
        if not result:
            print(f)
        else:
            for work in result:
                viaf_work_id = work.attrib.get('id')
                if viaf_work_id:
                    viaf_work_id = viaf_work_id.lstrip('VIAF|')
                title = work.find('ns1:title', ns).text
                sources = work.findall('ns1:sources/ns1:sid', ns)
                rism_source = None
                lc_source = None
                for source in sources:
                    if source.text.startswith('DE663|'):
                        rism_source = source.text.lstrip('DE663|')
                    if source.text.startswith('LC|'):
                        lc_source = source.text.lstrip('LC|')

                row = {
                    'rism_person_id': f.name.rstrip('.xml'),
                    'viaf_person_id': viaf_person_id.text,
                    'viaf_work_id': viaf_work_id,
                    'title': title,
                    'rism_source': rism_source,
                    'lc_source': lc_source,
                }
                data.append(row)
    return data


def get_argument_parser():
    parser = argparse.ArgumentParser(
        description='Get VIAF works from the VIAF person record',
    )
    parser.add_argument('filepath', help='A folder with viaf xml files')
    return parser


def main():
    parser = get_argument_parser()
    args = parser.parse_args()

    filepath = pathlib.Path(args.filepath)
    data = get_data(filepath)
    filename = 'viaf_works_{}.csv'.format(datetime.date.today())
    with open(filename, 'w') as fout:
        writer = csv.DictWriter(fout, data[0].keys(), dialect='excel-tab')
        writer.writeheader()
        writer.writerows(data)


if __name__ == '__main__':
    main()
